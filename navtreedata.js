/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Mechatronics Coursework and Term Project", "index.html", [
    [ "Coursework:", "index.html#sec_CW", [
      [ "Introduction", "index.html#subsec_intro", null ],
      [ "Motor Driver", "index.html#subsec_mot", null ],
      [ "Encoder Driver", "index.html#subsec_enc", null ],
      [ "Closed-Loop Proportional Controller", "index.html#subsec_prop", null ],
      [ "Sample Step Responses", "index.html#subsec_SampR", null ],
      [ "Inertial Measurement Unit", "index.html#subsec_IMU", null ],
      [ "IMU Manipulation", "index.html#subsec_imu", null ]
    ] ],
    [ "Project Proposal", "page_Proposal.html", [
      [ "Mechatronics Project Proposal: Do Nothing Box", "page_Proposal.html#sec_title", null ],
      [ "Problem Statement:", "page_Proposal.html#sec_problem", null ],
      [ "Bill of Materials:", "page_Proposal.html#sec_materials", null ],
      [ "Assembly Plan:", "page_Proposal.html#sec_plan", null ],
      [ "Safety Assessment:", "page_Proposal.html#sec_safety", null ],
      [ "General Timeline:", "page_Proposal.html#sec_timeline", null ]
    ] ],
    [ "Term Project: Do Nothing Box", "page_TermProject.html", [
      [ "Intoduction:", "page_TermProject.html#sec_int", null ],
      [ "Code Generation:", "page_TermProject.html#sec_code", null ],
      [ "Production:", "page_TermProject.html#sec_prod", null ],
      [ "Product:", "page_TermProject.html#sec_final", null ],
      [ "Component Issues:", "page_TermProject.html#sec_Prob", null ],
      [ "Conclusion:", "page_TermProject.html#sec_conc", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"En__coder_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';